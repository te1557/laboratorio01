package com.academiamoviles.ejemplo02android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
        btnProcesar.setOnClickListener {
            val edad = edtEdad.text.toString().toInt()

            val const = 18

            if(edad>=const){
                tvResultado.text = "Usted es mayor de edad"
            }else{
                tvResultado.text = "Usted es menor de edad"
            }
        }
        
    }
}